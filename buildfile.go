// +build mage

package main

import (
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Aliases for functions
var Aliases = map[string]interface{}{
	"Cleanup": Clean,
	"Lt":      LoveTest,
}

// Name of executable
const name = "./codego"

// A function that builds an executable and runs it
func Test() error {
	mg.Deps(Build)
	err := sh.RunV(name)
	if err != nil {
		return err
	}
	Clean()
	return err
}

// Builds the executable
func Build() error {
	err := sh.RunV("go", "build", "-o", name)
	return err
}

// Removes built files
func Clean() error {
	err := sh.Rm(name)
	return err
}

// Builds executable then deletes it
// Used for finding errors in code
func LoveTest() error {
	// Build executable
	buildErr := Build()

	// Remove executable
	// Note that Clean wont return an error if the executable doesn't exist
	// Meaning that if Build can't create the executable, Clean still wont fail
	cleanErr := Clean()

	if cleanErr != nil {
		return cleanErr
	}
	if buildErr != nil {
		return buildErr
	}
	return nil
}
