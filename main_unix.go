// +build linux darwin

package main

import (
	"os"
)

// On windows, symlink copies the files to a temporary directory where they need to be copied back later
// On unix, it simply creates a symbolic link
func SymLink(a, b string) error {
	return os.Symlink(a, b)
}
