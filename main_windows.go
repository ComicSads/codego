// +build windows
package main

import (
	"github.com/magefile/mage/sh"
	"os"
)

// On windows, symlink copies the files to a temporary directory where they need to be copied back later
// On unix, it simply creates a symbolic link
func SymLink(a, b string) error {
	// Complaints arise when copying to a file that doesn't exist
	f, err := os.Create(b)
	if err != nil {
		return err
	}
	f.Close()
	return sh.Copy(a, b)
}
