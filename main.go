package main

import (
	"fmt"
	"github.com/mattn/go-isatty"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

var program string
var args []string

func doErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "codego: %s\n", err)
		os.Exit(1)
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())

	program = "nvim"
	args = []string{}

	tmp, err := tempDir()
	doErr(err)
	defer os.RemoveAll(tmp)

	wd, err := os.Getwd()

	cmd := exec.Command("go", "list", "-f", "'{{.GoFiles}}'")
	b, err := cmd.Output()
	doErr(err)
	s := strings.TrimSpace(string(b))
	s = strings.Trim(s, "' []")
	files := strings.Fields(s)
	if !isTerm() {
		for _, f := range files {
			fmt.Println(f)
		}
		return
	}
	for _, f := range files {
		old := filepath.Join(wd, f)
		link := filepath.Join(tmp, f)
		err := SymLink(old, link)
		doErr(err)
		/*
			if runtime.GOOS == "windows" {
				defer func(){
					err := SymLink(link, old)
					doErr(err)
				}()
			}
		*/
	}

	args = append(args, tmp)
	cmd = exec.Command(program, args...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	err = cmd.Run()
	doErr(err)
	// Delete files
	if runtime.GOOS == "windows" {
		for _, f := range files {
			old := filepath.Join(wd, f)
			link := filepath.Join(tmp, f)
			err := SymLink(link, old)
			doErr(err)
		}
	}
}

const template = "codego-XXXXXXXX"

// Returns path to a directory
// Guarenteed to be empty and writeable
// Returns error if directory can't be found/isn't writeable
func tempDir() (string, error) {
	tmp := os.TempDir()
	if tmp == "" {
		return "", fmt.Errorf("Unable to find temporary directory!")
	}
	var s string
	for {
		s = randName()
		path := filepath.Join(tmp, s)
		t, err := fileExist(path)
		if err != nil {
			return "", err
		}
		if !t {
			break
		}
	}
	tmp = filepath.Join(tmp, s)
	os.MkdirAll(tmp, 0700)
	return tmp, nil
}

var fileChars = [64]rune{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '-', '_'}

// Returns a random string matching template
func randName() string {
	var b strings.Builder
	for _, r := range template {
		switch r {
		case 'X':
			b.WriteRune(fileChars[rand.Intn(len(fileChars))])
		default:
			b.WriteRune(r)
		}
	}
	return b.String()
}

// Returns true if file exists
func fileExist(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func isTerm() bool {
	if isatty.IsTerminal(os.Stdout.Fd()) {
		return true
	} else if isatty.IsCygwinTerminal(os.Stdout.Fd()) {
		return true
	} else {
		return false
	}

}
